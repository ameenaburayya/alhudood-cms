
export function getPosts () {
    return fetch('https://alhudood.net/wp-json/wp/v2/posts')
  }
  
  export function getPost (slug) {
    return fetch(`https://alhudood.net/wp-json/wp/v2/posts?slug=${slug}`)
  }
  