
import { Sidebar } from 'primereact/sidebar';
import NavMenu from '../../components/menus/navMenu'
import HamburgerIcon from '../../components/icons&Logos/hamburgerIcon'
import React, { useState } from 'react';
import { Drawer, Button } from 'antd';

export default function SideMenu() {
    const [visible, setVisible] = useState(false);

    const showDrawer = () => {
        setVisible(true);
    };

    const onClose = () => {
        setVisible(false);
    };

    return (
        <>
      <HamburgerIcon onClick={showDrawer} />
      <Drawer
        placement="right"
        closable={false}
        onClose={onClose}
        visible={visible}
      >
       <NavMenu />
      </Drawer>
    </>
    )
}
