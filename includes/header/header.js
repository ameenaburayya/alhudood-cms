import Image from 'next/image'
import Link from 'next/link'
import AlhudoodLogo from '../../components/logos/alhudoodLogo'
import HamburgerIcon from '../../components/icons/hamburgerIcon'
import { HeaderMenuList } from '../../lib/Menu'
import MenuList from '../../components/menus/menuList';
import SearchIcon from '../../components/icons/searchIcon';
import SideMenu from '../../components/menus/sideMenu'
import SecondaryButton from '../../components/buttons/secondaryButton'


export default function Header() {
    return (
        <nav className="header">
            <div className="header__container">
                <div className="header__logo">
                    <Link href="/">
                        <a>
                            <AlhudoodLogo />
                        </a>
                    </Link>
                </div>
                <div className="header__list">
                    <div className="header__buttons">
                        <div className="header__search">
                            <SearchIcon />
                        </div>
                        <div className="header__sidemenu">
                            <SideMenu />
                        </div>
                        
                        <SecondaryButton link={"/"} color="grey" label="تسجيل" />
                    </div>
                    <div className="header__menu">
                        <MenuList menu={HeaderMenuList} />
                    </div>
                </div>
            </div>
        </nav>
    );
}
