
import AlhudoodLogo from '../../components/logos/alhudoodLogo';
import SocialMedia from '../../components/social/socialMedia';
import Paragraph from '../../components/text/paragraph';
import {FooterMenu} from '../../lib/Menu'
import MenuList from '../../components/menus/menuList';
export default function Footer() {
    return (
        <footer>
            <div className="footer-inner">
                <div className="footer-about footer-section">
                    <div className="alhudood-logo">
                        <AlhudoodLogo white />
                    </div>
                    <Paragraph text="خسائر اللازمة ومطالبة حدة بل. الآخر الحلفاء أن غزو, إجلاء وتنامت عدد مع. لقهر معركة لبلجيكا، بـ انه, ربع الأثنان المقيتة في, اقتصّت المحور حدة و. هذه ما طرفاً عالمية استسلام, الصين وتنامت حين ٣٠, ونتج والحزب المذابح كل جوي." />
                </div>
                <div className="footer-newsletter footer-section">
                    
                </div>
                <div className="footer-menu footer-section">
                    <MenuList menu={FooterMenu} />
                </div>
                <div className="footer-social footer-section"><SocialMedia color={"white"} /></div>
            </div>
            <div className="footer-copyright">
               
            </div>
        </footer>
    )
}
