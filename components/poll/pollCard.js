import React, { Component } from 'react';
import Poll from 'react-polls';
 
// Declaring poll question and answers
const pollQuestion = 'ما هي الطريقة الأمثل لتخليص بلدك من الديون المتراكمة؟'
const pollAnswers = [
  { option: 'احتلال بلد نفطي', votes: 8 },
  { option: 'إيقاف الخدمات الهامشية كالصحة والتعليم', votes: 2 },
  { option: 'التوجه نحو زراعة الأمل والطموح', votes: 2 }
]
 
class PollCard extends Component {
  // Setting answers to state to reload the component with each vote
  state = {
    pollAnswers: [...pollAnswers]
  }
 
  // Handling user vote
  // Increments the votes count of answer when the user votes
  handleVote = voteAnswer => {
    const { pollAnswers } = this.state
    const newPollAnswers = pollAnswers.map(answer => {
      if (answer.option === voteAnswer) answer.votes++
      return answer
    })
    this.setState({
      pollAnswers: newPollAnswers
    })
  }
 
  render () {
    const { pollAnswers } = this.state
    return (
        <div className="poll-card">
          <Poll  question={pollQuestion} answers={pollAnswers} onVote={this.handleVote} />
        </div>
    );
  }
};


export default PollCard