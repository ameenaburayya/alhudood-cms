import Footer from '../../includes/footer/footer'
import Header from '../../includes/header/header'
import Meta from '../meta/meta'
import Head from 'next/head'
import React from 'react'



export default function PostLayout({ children, title, description, image, id }) {
  return (
    <React.Fragment>
        <Header />
        <div className="post">
            <main>{children}</main>
        </div>
        <Footer />
    </React.Fragment>
  )
}
