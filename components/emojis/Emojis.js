import React from 'react';
import Component from 'react';
import CryingEmoji from './cryingEmoji'
import LaughingEmoji from './laughingEmoji'
import FireEmoji from './fireEmoji'
import PoopEmoji from './poopEmoji'

class Reacting extends React.Component {

    state = {
      alreadyReacted: this.props.emojis.alreadyReacted,
      cry: this.props.emojis.cry,
      fire: this.props.emojis.fire,
      laugh: this.props.emojis.laugh,
      poop: this.props.emojis.poop,
      currentReaction: this.props.emojis.alreadyReacted
    }
  


  handleReaction = (reaction) => {

    if (this.state.alreadyReacted === false){
      this.setState({
        alreadyReacted: true,
        [reaction]: this.state[reaction] + 1,
        currentReaction: [reaction]
      })
    } else if (this.state.currentReaction == reaction) {
        this.setState({
          alreadyReacted: false,
          [reaction]: this.state[reaction] - 1,
          currentReaction: null
        })
      } else{
        this.setState({
          alreadyReacted: true,
          [reaction]: this.state[reaction] + 1,
          [this.state.currentReaction]: this.state[this.state.currentReaction] - 1,
          currentReaction: [reaction]
        })
      }
  }

  
  render() {
    return (
      <div className="emojis">
        <ul className={this.state.alreadyReacted ? 'reacted' : 'unreacted'}>
        {/* colored={this.state.currentReaction == "fire" ? colored : null} */}
            <li className={this.state.currentReaction == "fire" ? "active" : null}>
              <button onClick={ () => this.handleReaction("fire") }>
                <FireEmoji colored={this.state.currentReaction == "fire" ? true : false}  />
                {this.state.fire ? <div className="emoji-counter">{this.state.fire}</div> : null}
              </button>
            </li>
            <li className={this.state.currentReaction == "cry" ? "active" : null}>
              <button onClick={ () => this.handleReaction("cry") }>
                <CryingEmoji colored={this.state.currentReaction == "cry" ? true : false}  />
                {this.state.cry ? <div className="emoji-counter">{this.state.cry}</div> : null}
              </button>
            </li>
            <li className={this.state.currentReaction == "laugh" ? "active" : null}>
              <button  onClick={ () => this.handleReaction("laugh") }>
                <LaughingEmoji colored={this.state.currentReaction == "laugh" ? true : false}  />
                {this.state.laugh ? <div className="emoji-counter">{this.state.laugh}</div> : null}
              </button>
            </li>
            <li className={this.state.currentReaction == "poop" ? "active" : null}>
              <button onClick={ () => this.handleReaction("poop") }>
                <PoopEmoji colored={this.state.currentReaction == "poop" ? true : false}  />
                {this.state.poop ? <div className="emoji-counter">{this.state.poop}</div> : null}
              </button>
            </li>
        </ul>
      </div> 
    );
  }
}

export default Reacting;