import Drawer from '@material-ui/core/Drawer';
import MenuList from './menuList'
import { SideMenuList, SideMenuSecList } from '../../lib/Menu'
import React from 'react';
import HamburgerIcon from '../icons/hamburgerIcon';
import SocialMedia from '../social/socialMedia';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Dal from '../icons/dal'

export default function SideMenu() {

    const [open, setOpen] = React.useState(false);

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };


    return (
        <div className="side-menu">
            <HamburgerIcon onClick={handleDrawerOpen} />
            <Drawer anchor="right" open={open} onClose={handleDrawerClose}>
                <div className="side-menu-inner">
                    <Dal />
                    <IconButton className="close-button" size="small" aria-label="close" color="inherit" onClick={handleDrawerClose}>
                        <CloseIcon fontSize="small" />
                    </IconButton>
                    <div className="main-list">
                        <MenuList menu={SideMenuList} />
                    </div>
                    <div className="sec-list">
                        <MenuList menu={SideMenuSecList} />
                    </div>
                    <SocialMedia />
                </div>
            </Drawer>
        </div>
    );
}