import React from 'react';
import Link  from 'next/link';


export default function MenuList(menu) {
    const temp = Object.values(menu)
    const list = temp[0]
    return (
        <ul>
            {list.map((item, i) =>
                <li key={i}>
                    <Link href={`${item.link}`}>
                        <a>{item.label}</a>
                    </Link>
                </li>   
            )}
        </ul>
    )
}