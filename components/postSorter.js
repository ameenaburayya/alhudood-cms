import React, { Component } from 'react'
import SecPosts from './posts/secPosts';
import MainPost from './posts/mainPost';
import PostBlock from './posts/postBlock';
import DropDown from './dropDown';



export default class Sorter extends Component {

    constructor(props){
        super(props)
        this.sortByDate = this.sortByDate.bind(this)
        this.sortByReading = this.sortByReading.bind(this)
        this.handleSort = this.handleSort.bind(this)
        this.state = {
            postList: [],
            filter: null,
            order: null,
            isOpen: false
        }

    }

    sortByDate () {
        const postList = this.props.data
        let newPostList = postList.sort((a, b) => {
            return new Date(b.date) - new Date(a.date);
        })

        this.props.onChangeValue(newPostList)
        this.setState({
          filter: null,
          order: "date",
        })
    }

    sortByReading(){
        const postList = this.props.data
        let newPostList = postList.sort((a,b) => {
            return b.reads - a.reads;
        })
        this.props.onChangeValue(newPostList)
        this.setState({
            filter: null,
            order: "reading"
        })
    }

    getPostsWithType(value){
        this.setState({
            postList: this.props.data
        })
        
    }

    handleSort(value){
        if (value == "date"){
            this.sortByDate();
        }
        else if (value == "most_reading"){
           this.sortByReading();
        }
        else{
            this.setState({
                filter: value,
                postList: this.props.data
            })
        }
    }

    toggleDropdown(){
        this.setState({
            isOpen: !this.state.isOpen
        })
    }


    componentDidMount() {
        // this.setState({
        //   postList:  this.props.data
        // })
        this.sortByDate
    }

    render() {
        const options = [
            {
                name: "الأحدث",
                value: "date"
            },
            {
                name: "الأكثر قراءة",
                value: "most_reading"
            }
        ]
        if (this.props.types){
            for (var i = 0; i < this.props.types.length; i++){
                var temp_obj = {};
                var temp_type = this.props.types[i];
                options.push({"name": temp_type.name, "value": temp_type.slug})
            }
        }
        return (
            <>
                <div className="sorting-dropdown">
                    <DropDown onClick={(e) => this.handleSort(e)} options={options} />
                </div>
                {/* <div className="sorted-posts">
                    {(()=> {
                        if (this.state.filter) {
                            const filtered = posts.filter(post => post.content_type.slug === this.state.filter)
                            if(filtered.length > 0){
                                return(
                                    // <SecPosts posts={filtered} />
                                    filtered.map((post, index) =>{
                                        return(
                                            <PostBlock archive image_shape="wide" post={post} key={index} />
                                        )
                                    })
                                )
                            }
                            else{
                                return(<div>لا يوجد نتائج</div>)
                            }
                        } else if(this.state.order === "date"){
                            return(
                                // <SecPosts posts={posts} />
                                posts.map((post, index) =>{
                                    return(
                                        <PostBlock archive image_shape="wide" post={post} key={index} />
                                    )
                                })
                            )   
                        } else if(this.state.order === "reading"){
                            return(
                                // <SecPosts posts={posts} />
                                posts.map((post, index) =>{
                                    return(
                                        <PostBlock archive image_shape="wide" post={post} key={index} />
                                    )
                                })
                            )  
                        }
                    })()}
                </div> */}
            </>
        )
    }
}
