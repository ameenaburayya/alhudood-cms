import React from 'react'
import SocialShare from '../social/socialShare';
import Comments from '../comments/comments';
import HashIcon from '../icons/hashIcon';
import Reacting from '../emojis/emojis';
import SecHeading from '../headings/secondaryHeading';
import SocialMobileShare from '../social/socialMobileShare';
import DalHeading from '../headings/dalHeading';
import PrimaryButton from '../buttons/primaryButton';
import LinkIcon from '../icons/linkIcon';
import SecondaryButton from '../buttons/secondaryButton';
import MainHeading from '../headings/MainHeading';
import {getImageUrl} from '@takeshape/routing'

export default class PostBody extends React.Component{

    constructor(props) {
        super(props);
    }

    render(){
        return (
            <div className="post_container">
                <section className="post-image">
                    <figure>
                        <picture>
                            <img src={getImageUrl(this.props.post.featuredImage.path)}></img>
                        </picture>
                    </figure>
                </section>
                
                {/* <section id="post-content" className="post-content" dangerouslySetInnerHTML={{ __html: this.props.post.content }}>
                </section> */}

                <section id="post-content" className="post-content">
                    {this.props.post.content.blocks.map((block) => 
                        <p className={block.type == "blockquote" ? "quote" : ''}>{block.text}</p>
                    )}
                </section>

                <section className="post-extra">
                    <div className="post-tags">
                        {this.props.post.tags.length
                        ?  <> 
                                <div className="tags-icon">
                                    <HashIcon />
                                </div>
                                <div className="tags-list">
                                    <ul>
                                        {this.props.post.tags.map((tag) =>   
                                            <li key={tag.id}>{tag.name}</li>
                                        )}
                                    </ul>
                                </div>
                            </>
                        :   null
                        }
                        
                        
                    </div>
                    <div className="post-emojis">
                        <SecHeading label="شعورك تجاه المقال؟" />
                        <Reacting emojis={this.props.post.emojis} />
                    </div>
                </section>
                <section className="post-share">
                    <div className="desktop-share">
                        <SocialShare id={this.props.post.id} />
                    </div>
                    <SocialMobileShare id={this.props.post.id} postTitle={this.props.post.title}/>
                </section>

                <section className="post-comments">
                    <DalHeading label="تعليقات من المجتمع" />
                    <MainHeading label="تعليقات من المجتمع" withDash />
                    <Comments comments={this.props.post.comments} />
                    <SecondaryButton color="black" link="#" icon={<LinkIcon />} label="تابع النقاش على المجتمع" />
                </section>

            </div>
        );
    }
}
