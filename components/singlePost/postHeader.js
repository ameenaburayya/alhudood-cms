
import BookmarkButton from '../buttons/bookmarkButton'
import CommentsButton from '../buttons/commentsButton'


export default function PostHeader({title , correspondent, date, type, bookmark, id}) {
    return (
        <header className="post-header">
            <div className="post-type">{type}</div>
            <h1 className="post-title">{title}</h1>

            {correspondent.name.length ? <p className="post-correspondent"><span>{correspondent.name}،</span> {correspondent.description}</p> : null}
            
            <p className="post-date">{date}</p>
            <div className="post-buttons">
                <ul>
                    <li className="bookmark">
                        <BookmarkButton id={id} bookmark={bookmark} />
                    </li>
                    <li className="comments">
                        <CommentsButton />
                    </li>
                </ul>
            </div>
        </header>
    );
}
