
import Link from 'next/link'
import LinesEllipsis from 'react-lines-ellipsis'
import Truncate from 'react-truncate';
import HashIcon from '../icons/hashIcon'
import HashIconFilled from '../icons/hashIconFilled';
import {getImageUrl} from '@takeshape/routing'


export default function RelatedPosts({ posts }) {
    return (
        <section className="related-posts">
            <div className="inner">
                {posts.map( (post) => 
                    <article className="related-post" key={post._id}>
                        <div className="post-image">
                            <figure>
                                <Link as={`/${post.category.slug}/${post.contentType.slug}/${post._id}`} href="/[category]/[contentType]/[slug]">
                                    <picture>
                                        <img src={getImageUrl(post.featuredImage.path)}></img>
                                    </picture>
                                </Link>
                            </figure>
                        </div>
                        <div className="post-details">
                            <div className="post-type">نوع المحتوى</div>
                            <div className="post-title">
                                <h2>
                                    <Link as={`/${post.category.slug}/${post.contentType.slug}/${post._id}`} href="/[category]/[contentType]/[slug]"> 
                                        <Truncate lines={2} ellipsis={<span className="ellipsis">...</span>}>
                                            {post.title}
                                        </Truncate>
                                    </Link>
                                </h2>
                            </div>
                            <div className="post-excerpt">
                            
                                <Link as={`/${post.category.slug}/${post.contentType.slug}/${post._id}`} href="/[category]/[contentType]/[slug]">
                                    <Truncate lines={2} ellipsis={<span className="ellipsis">...</span>}>
                                        {post.excerpt}
                                    </Truncate>
                                </Link>

                            </div>
                            <div className="post-tags">
                                {post.tags.length 
                                ? <>
                                    <HashIconFilled />
                                    {post.tags.map((tag, i) => 
                                        <div key={tag._id} className="post-tag">{tag.name}</div>
                                    )}
                                </>
                                : null
                                }
                                
                            </div>
                        </div>
                    </article>
                )}
            </div>
        </section>
    )
}
