import React, { Component } from 'react'
import CommentAvatar from '../icons/commentAvatar'
import ReadMore from '../readMore';
import {getImageUrl} from '@takeshape/routing'


export default class Comment extends Component {
    render() {
        return (
            <div className="comment">
                <div className="commenter-details">
                    <div className="commenter-avatar">
                        {this.props.comment.image
                        ? <img src={getImageUrl(this.props.comment.image)}></img>
                        : <CommentAvatar />
                        }
                    </div>
                    <div className="commenter-name">
                        {this.props.comment.name}
                    </div>
                </div>
                <div className="comment-wrapper">
                    <div className="comment-content">
                        <ReadMore lines={6} more="المزيد" less="اقل">
                            {this.props.comment.comment}
                        </ReadMore> 
                    </div>
                    <img className="mobile" src="/Assets/UI_Elements/comment-line-mob.svg"></img>
                    <img className="desktop" src="/Assets/UI_Elements/comment-line-desk.svg"></img>

                </div>
            </div>
        )
    }
}
