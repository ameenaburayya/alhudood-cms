
import Ticker from 'react-ticker'

import { isMobile } from 'react-device-detect';


export default function TagsTicker({postTags}) {
    return (
        <>
            <Ticker direction="toRight" speed={3}>
                {() => (
                    <>
                        <div className="tags-line">
                            {isMobile 
                            ? <>
                                {postTags.map((tag, index) => {
                                    if(index % 2 == 0){
                                        return <span key={index}>{tag.name}</span>
                                    }
                                })}
                            </>
                            : <>
                                {postTags.map((tag, index) => {
                                    return <span key={index}>{tag.name}</span>
                                })}
                            </>
                            }
                        </div>
                    </>
                )}
            </Ticker>
            <Ticker direction="toRight" speed={3}>
                {() => (
                    <>
                        <div className="tags-line">
                            {postTags.map((tag, index) => {
                                if(index % 2 != 0){
                                    return <span key={index}>{tag.name}</span>
                                }
                            })}
                        </div>
                        
                    </>
                )}
            </Ticker>
        </>
    )
}
