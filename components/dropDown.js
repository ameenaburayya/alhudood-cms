import React, { useState } from "react";
import ClickAwayListener from '@material-ui/core/ClickAwayListener';

export default function DropDown({options, onClick}) {
    const [isOpen, setIsOpen] = useState(false);
    const [selectedOption, setSelectedOption] = useState(null);

    const toggling = () => setIsOpen(!isOpen);

    const onOptionClicked = (option) => {
        setSelectedOption(option.name);
        setIsOpen(false);
    };
    const handleClickAway = () => {
        setIsOpen(false);
    }

    return (
        <ClickAwayListener onClickAway={handleClickAway}>
            <div className="dropdown">
                <div className={`dropdown-header${isOpen ? " open" : ""}`} onClick={toggling}>
                        <div className="dropdown-header-arrow">

                        </div>
                        <div className="dropdown-header-selected">
                            <span>{selectedOption || "اختر"}</span>
                        </div>
                </div>
                <div className={`dropdown-list${isOpen ? " open" : ""}`}>
                    <ul>
                    {options.map(option => (
                        <li className={selectedOption == option ? "active" : ""} 
                        onClick={()=> {
                            onClick(option.value);
                            onOptionClicked(option)
                        }} 
                        key={Math.random()}>
                            <span>{option.name}</span> 
                        </li>
                    ))}
                    </ul>
                </div>
            </div>
        </ClickAwayListener>
    );
  }