import React, { Component, useState, useEffect} from 'react';
import Link from 'next/link'
import LinesEllipsis from 'react-lines-ellipsis'
import HashIconFilled from '../icons/hashIconFilled';
import BookmarkButton from '../buttons/bookmarkButton';
import CommentsButton from '../buttons/commentsButton';
import SkeletonCard from '../skeletons/postBlockSkeleton'
import {getImageUrl} from '@takeshape/routing'

export default function PostBlock({post, image_shape, archive, excerpt}) {

    const [postData, setPost] = useState();
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        setLoading(true);
        const timer = setTimeout(() => {
            setPost(post);
            setLoading(false);
        }, 1000);
        // Cancel the timer while unmounting
        return () => clearTimeout(timer);
    }, []);

    return (
        <>
        {loading && <SkeletonCard />}
        {!loading && postData
        ?
           <> {
                archive 
                ? <>
                    <article className={`post-item${archive ? " archive" : ""} ${image_shape ? image_shape : ""}`}>
                        <div className="post__image">
                            <figure>
                                <Link as={`/${post.category.slug}/${post.contentType.slug}/${post.id}`} href="/[category]/[contentType]/[slug]">
                                    <picture>
                                        <img src={getImageUrl(post.featuredImage.path)} alt=""/>
                                    </picture>
                                </Link>
                            </figure>
                        </div>
                        <div className="post__title">
                            <div className="post__contentType">{post.contentType.name}</div>
                            <h2>
                                <Link as={`/${post.category.slug}/${post.contentType.slug}/${post.id}`} href="/[category]/[contentType]/[slug]"> 
                                    <LinesEllipsis
                                    text={post.title}
                                    maxLine='3'
                                    ellipsis='...'
                                    trimRight
                                    />
                                </Link>
                            </h2>
                        </div>
                        <div className="post__details">
                            <div className="post__meta">
                                <div className="post__date">{post.date_ar}</div>
                                <div className="post__tags">
                                    {post.tags.length 
                                    ? <>
                                        <HashIconFilled />
                                        {post.tags.map((tag, i) => 
                                            <div key={tag.id} className="post-tag">{tag.name}</div>
                                        )}
                                    </>
                                    : null
                                    }
                                </div>
                            </div>
                            <div className="post__buttons">
                                <BookmarkButton />
                                <CommentsButton />
                            </div>
                        </div>
                    </article>
                </>
                : <>
                    <article className={`post-item ${image_shape ? image_shape : ""}`}>
                        <div className="post__image">
                            <figure>
                                <Link as={`/${post.category.slug}/${post.contentType.slug}/${post.id}`} href="/[category]/[contentType]/[slug]">
                                    <picture>
                                        <img src={getImageUrl(post.featuredImage.path)} alt=""/>
                                    </picture>
                                </Link>
                            </figure>
                        </div>
                        <div className="post__details">
                            <div className="post__type">{post.category.name}</div>
                            <div className="post__title">
                                <h2>
                                    <Link as={`/${post.category.slug}/${post.contentType.slug}/${post.id}`} href="/[category]/[contentType]/[slug]"> 
                                        <LinesEllipsis
                                        text={post.title}
                                        maxLine='3'
                                        ellipsis='...'
                                        trimRight
                                        />
                                    </Link>
                                </h2>
                            </div>
                            {excerpt
                            ? <div className="post__excerpt">
                                    <Link as={`/${post.category.slug}/${post.contentType.slug}/${post.id}`} href="/[category]/[contentType]/[slug]">
                                    <LinesEllipsis
                                    text={post.excerpt}
                                    maxLine='2'
                                    ellipsis='...'
                                    trimRight
                                    />
                                    </Link>
                            </div>
                            : null
                            }
                            <div className="post__tags">
                                {post.tags.length 
                                ? <>
                                    <HashIconFilled />
                                    {post.tags.map((tag, i) => 
                                        <div key={tag.id} className="post-tag">{tag.name}</div>
                                    )}
                                </>
                                : null
                                }
                            </div>
                        </div>
                    </article>
                </>
            }</>
        : null
        }
        </>
        
    )
}

