import React, { Component } from 'react';
import MainPost  from './mainPost';
import SecPosts from './secPosts';
import { getPosts } from '../../api/post'


function LatestPosts({posts}) {

  const mainPost = posts[0]

  const secPosts = posts.slice(1)

    return(
        <React.Fragment>
            {mainPost && (
            <MainPost
            post={mainPost}
            />
            )}
            {secPosts.length > 0 && <SecPosts posts={secPosts} />}
        </React.Fragment>
    )
}


export default LatestPosts;
