import React, { Component } from 'react'
import Link from 'next/link'
import HashIcon from '../icons/hashIcon';
import { getImageUrl } from '@takeshape/routing'

export default function SecPosts(posts) {

    const secPosts = Object.values(posts)[0];
    console.log(secPosts)
        return (
            <div className="sec-posts">
                {secPosts.map((post) =>
                    <div key={post.id} className="sec-post">
                        <div className="post-image">
                            <Link as={`/${post.category.slug}/${post.contentType.slug}/${post.id}`} href="/[category]/[contentType]/[slug]">
                                <img
                                    src={getImageUrl(post.featuredImage.path)}
                                />
                            </Link>
                        </div>
                        <div className="post-details">
                            <div className="post-type">
                                {post.category.name}
                            </div>
                            <div className="post-title">{post.title} </div>
                            <div className="post-tags">
                                {post.tags.length
                                ? <HashIcon />
                                : null
                                }
                                <ul>
                                    {post.tags.map((tag) =>
                                        <li key={tag._id} className="post-tag">{tag.name}</li>
                                    )}
                                </ul>
                                
                            </div>
                        </div>
                    </div>
                )}
            </div>
            
            
        )
}