import React, { Component } from 'react';
import PostBlock from './postBlock';

export default function PostsBlock({archive, data , image_shape}) {
    return (
        <>
        {data
        ? <>
            {data.map((post,index) => {
                return(
                    <PostBlock key={index} post={post} image_shape={image_shape} archive={archive} />
                )
            })}
        </>
        : <h1>no data</h1>
        }
        </>
        
    )
}

