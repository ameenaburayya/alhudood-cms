import React, { Component } from 'react';
import Link from 'next/link'
import { getImageUrl } from '@takeshape/routing'

export default function MainPost({post}) {
    post['id'] = post._id
    console.log(post)
    return (
        <div className="main-post">
            <div className="post__image">
                <Link as={`/${post.category.slug}/${post.contentType.slug}/${post.id}`} href="/[category]/[contentType]/[slug]">
                    {/* <Image src={post.featuredImage.path} /> */}
                    <img
                        src={getImageUrl(post.featuredImage.path)}
                    />
                </Link>
            </div>
            <div className="post__details">
            
                <div className="post__type">{post.category.name}</div>
                
                <Link as={`/${post.category.slug}/${post.contentType.slug}/${post.id}`} href="/[category]/[contentType]/[slug]">
                    <div className="post__title">{post.title}</div>
                </Link>
                <div className="post__expert">
                    {post.excerpt}
                </div>
                <div className="post__terms">
                </div>
            </div>
        </div>
    )
}

