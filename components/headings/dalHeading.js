import React, { Component } from 'react'
import Dal from '../icons/dal';

export default class DalHeading extends Component {
    render() {
        return (
            <div className="dal-heading">
                <Dal />
                <h2>{this.props.label}</h2>
            </div>
        )
    }
}
