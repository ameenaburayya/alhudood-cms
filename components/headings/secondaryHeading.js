import React, { Component } from 'react'

export default class SecHeading extends Component {
    render() {
        return (
            <p className="sec-heading">{this.props.label}</p>
        )
    }
}
