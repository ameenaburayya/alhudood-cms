import React, { Component } from 'react'

export default class MainHeading extends Component {
    render() {
        return (
            <div className={`main-heading${this.props.withDash ? " with-dash": ""}`}>
                <h2>{this.props.label}</h2>
            </div>
            
        )
    }
}
