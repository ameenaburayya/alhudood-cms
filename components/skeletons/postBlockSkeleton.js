import React from "react"
import ContentLoader from "react-content-loader"

const SkeletonCard = (props) => (
    <div className="skeleton">
        <ContentLoader 
        speed={2}
        width={257}
        height={103}
        viewBox="0 0 257 103"
        backgroundColor="#f3f3f3"
        foregroundColor="#ecebeb"
        {...props}
    >
        <path d="M 128.7 82.1 H 1 c -0.6 0 -1 -0.4 -1 -1 V 1 c 0 -0.6 0.4 -1 1 -1 h 127.8 c 0.6 0 1 0.4 1 1 v 80.1 c -0.1 0.6 -0.5 1 -1.1 1 z M 9 103 H 1 c -0.6 0 -1 -0.4 -1 -1 V 89 c 0 -0.6 0.4 -1 1 -1 h 8 c 0.6 0 1 0.4 1 1 v 13 c 0 0.5 -0.4 1 -1 1 z M 23 103 h -8 c -0.6 0 -1 -0.4 -1 -1 V 89 c 0 -0.6 0.4 -1 1 -1 h 8 c 0.6 0 1 0.4 1 1 v 13 c 0 0.5 -0.4 1 -1 1 z M 213.8 0 H 257 v 7 h -43.2 z M 156.1 15.5 H 257 v 11.6 H 156.1 z M 191 33.1 h 66 v 11.6 h -66 z" />
    </ContentLoader>
  </div>
)

export default SkeletonCard
