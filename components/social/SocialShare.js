import ClipboardShare from './clipboardShare'
import FacebookShare from './facebookShare';
import TwitterShare from './twitterShare';



export default function SocialShare({id}){

    return (
        <ul className="social-icons">
            <li><FacebookShare id={id} /></li>
            <li><TwitterShare id={id} /></li>
            <li><ClipboardShare id={id} /></li>
        </ul>
    )
}
