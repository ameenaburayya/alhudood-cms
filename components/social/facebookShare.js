import {CMS_URL, URL_SLUG} from '../../lib/constants'
import FacebookIcon from '../icons/socialIcons/facebookIcon'



export default function FacebookShare({id}){

    return (
        <>
            <a target="_blank" href={"https://facebook.com/sharer/sharer.php?u=" + CMS_URL + "posts/" + id}><FacebookIcon /></a>
        </>
    )
}
