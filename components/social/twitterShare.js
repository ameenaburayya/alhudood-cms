import {CMS_URL, URL_SLUG} from '../../lib/constants'
import TwitterIcon from '../icons/socialIcons/twitterIcon'



export default function TwitterShare({id}){

    return (
        <>
            <a target="_blank" href={"https://twitter.com/intent/tweet?url=https%3A//"+ URL_SLUG + "/posts/" + id}><TwitterIcon /></a>
        </>
    )
}
