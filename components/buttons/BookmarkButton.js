import BookmarkIcon from '../icons/bookmarkIcon';
import React from 'react';
import Component from 'react';
import axios from 'axios'

import Alert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';


class BookmarkButton extends React.Component {
    state = {
        bookmark: this.props.bookmark,
        open: false
    }

    handleBookmark = () => {
        axios.post(`https://staging.alhudoodcms.alhudoodnet.com/api/tv1/articles/${this.props.id}/bookmark`, {
            bookmark: !this.state.bookmark,
          })
          .then((response) => {
            if (response.status == 200){
                this.setState({
                    bookmark: !this.state.bookmark,
                    open: true
                })
            }
          }, (error) => {
            console.log(error);
          });
    }

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        this.setState({
            open: false
        })
    };

    render(){
        return (
            <>
                <button className={this.state.bookmark ? "active" : null} onClick={this.handleBookmark}>
                    <BookmarkIcon filled={this.state.bookmark ? true : false} />
                </button>
                <Snackbar
                anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
                }}
                open={this.state.open}
                onClose={this.handleClose}
                autoHideDuration={3000}
                >
                    <Alert icon={false} variant="filled" onClose={this.handleClose} severity={this.state.bookmark ? "success" : "error"}>
                        {this.state.bookmark ? "تم حفظ المقال" : "تم ازالة المقال"}
                    </Alert>  
                </Snackbar>
          </>
        );
    }
}


export default BookmarkButton;
