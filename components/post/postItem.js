import React, { Component } from 'react'
import { Link } from 'next'
import LinesEllipsis from 'react-lines-ellipsis'
import HashIconFilled from '../icons/hashIconFilled';


export default class PostItem extends Component {
  render(){
    const post = this.props.post
    return(
      <article className="post">
        <div className="post-image">
            <figure>
                <Link as={`/${post.category.slug}/${post.content_type.slug}/${post.id}`} href="/[category]/[contentType]/[slug]">
                    <picture>
                        <img src={post.image_link}></img>
                    </picture>
                </Link>
            </figure>
        </div>
        <div className="post-details">
            <div className="post-type">نوع المحتوى</div>
            <div className="post-title">
                <h2>
                    <Link as={`/${post.category.slug}/${post.content_type.slug}/${post.id}`} href="/[category]/[contentType]/[slug]"> 
                        <LinesEllipsis
                        text={post.title}
                        maxLine='3'
                        ellipsis='...'
                        trimRight
                        />
                    </Link>
                </h2>
            </div>
            <div className="post-excerpt">
            
                <Link as={`/${post.category.slug}/${post.content_type.slug}/${post.id}`} href="/[category]/[contentType]/[slug]">
                    <LinesEllipsis
                    text={post.excerpt}
                    maxLine='2'
                    ellipsis='...'
                    trimRight
                    />
                </Link>
            </div>
            <div className="post-tags">
                {post.tags.length 
                ? <>
                    <HashIconFilled />
                    {post.tags.map((tag, i) => 
                        <div key={tag.id} className="post-tag">{tag.name}</div>
                    )}
                </>
                : null
                }
                
            </div>
        </div>
      </article>
    )
  }
}

