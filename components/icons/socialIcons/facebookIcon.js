import React, { Component } from 'react'

export default class FacebookIcon extends Component {
    render() {
        return (
            <svg 
            fill={this.props.color}
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 25.201 25.048"
            >
            <path
                d="M-998.8 410.6a12.6 12.6 0 00-12.6-12.6 12.6 12.6 0 00-12.6 12.6 12.6 12.6 0 0010.632 12.447v-8.805h-3.2V410.6h3.2v-2.776c0-3.158 1.881-4.9 4.759-4.9a19.377 19.377 0 012.821.246v3.1h-1.589a1.821 1.821 0 00-2.053 1.968v2.362h3.495l-.559 3.642h-2.936v8.805A12.6 12.6 0 00-998.8 410.6z"
                transform="translate(1024 -398)"
            ></path>
            </svg>
        )
    }
}
