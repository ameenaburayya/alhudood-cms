import React, { Component } from 'react'

export default class MailIcon extends Component {
    render() {
        return (
            <svg
            x="0"
            y="0"
            fill={this.props.color}
            enableBackground="new 0 0 24 24"
            viewBox="0 0 24 24"
            >
            <path d="M21.8 3H2.2C1 3 0 4 0 5.2v12.2c0 2 1.6 3.6 3.5 3.6h17c1.9 0 3.5-1.6 3.5-3.5V5.2C24 4 23 3 21.8 3zm-1.4 2l-8.1 7.3c-.2.2-.5.2-.7 0L3.6 5h16.8zm.1 14h-17c-.8 0-1.5-.7-1.5-1.5V6.3l8.3 7.6c.9.9 2.4.9 3.4 0L22 6.3v11.2c0 .8-.7 1.5-1.5 1.5z"></path>
            </svg>
        )
    }
}
