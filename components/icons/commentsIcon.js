
import React from 'react';



export default class CommentsIcon extends React.Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            x="0"
            y="0"
            enableBackground="new 0 0 22.9 20.5"
            version="1.1"
            viewBox="0 0 22.9 20.5"
            xmlSpace="preserve"
            >
            <path
                d="M22.7.2c-.1-.1-.3-.2-.5-.2H.6C.3 0 0 .3 0 .6v13.1c0 .3.3.6.6.6h6.7l3.5 5.8c0 .1.1.1.2.2l.1.1c.1 0 .2.1.3.1.1 0 .2 0 .3-.1l.2-.1.1-.1 3.5-5.9h6.7c.3 0 .6-.3.6-.6V.7c.1-.2 0-.4-.1-.5zm-1.1 1.1V13h-6.5l-.2.1s-.1.1-.2.1l-3.2 5.3-3.2-5.2-.3-.1-.1-.1H1.3V1.3h20.3z"
                className="st0"
            ></path>
            <path
                d="M5.9 5.5H17c.4 0 .6-.3.6-.6s-.3-.7-.6-.7H5.9c-.4 0-.6.3-.6.6s.2.7.6.7zM5.9 10.1h5.3c.4 0 .6-.3.6-.6s-.3-.6-.6-.6H5.9c-.4 0-.6.3-.6.6s.2.6.6.6z"
                className="st0"
            ></path>
            </svg>
        )
    }
}