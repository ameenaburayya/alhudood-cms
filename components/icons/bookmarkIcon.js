
export default function BookmarkIcon({filled}) {
    return (
        <>
            {filled
            ? <>
                <svg
                xmlns="http://www.w3.org/2000/svg"
                x="0"
                y="0"
                enableBackground="new 0 0 13.9 20.6"
                version="1.1"
                viewBox="0 0 13.9 20.6"
                xmlSpace="preserve"
                >
                <path
                    d="M13.1 0H.7C.3 0 0 .3 0 .7v19.2c0 .2.1.4.2.5.1.1.3.2.5.2.1 0 .3 0 .4-.1l5.8-4 5.8 4c.3.2.8.1 1-.2.1-.1.1-.3.1-.4V.7c.1-.4-.3-.7-.7-.7z"
                    className="st0"
                ></path>
                </svg>
            </>
            : <>
                <svg
                xmlns="http://www.w3.org/2000/svg"
                x="0"
                y="0"
                enableBackground="new 0 0 13.9 20.6"
                version="1.1"
                viewBox="0 0 13.9 20.6"
                xmlSpace="preserve"
                >
                <path
                    d="M13.1 0H.7C.3 0 0 .3 0 .7v19.2c0 .2.1.4.2.5.1.1.3.2.5.2.1 0 .3 0 .4-.1l5.8-4 5.8 4c.3.2.8.1 1-.2.1-.1.1-.3.1-.4V.7c.1-.4-.3-.7-.7-.7zm-.7 1.4v17.1L7.3 15c-.2-.2-.6-.2-.8 0l-5.1 3.5V1.4h11z"
                    className="st0"
                ></path>
                </svg>
            </>
            }
        </>
    )
}