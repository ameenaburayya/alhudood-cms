
import React from 'react';

export default class ExpandIcon extends React.Component {
    render() {
        return (
            <svg
            xmlns="http://www.w3.org/2000/svg"
            width="17.751"
            height="8.666"
            viewBox="0 0 17.751 8.666"
            >
            <path
                fill="#afb7c4"
                d="M15.842 37.637a.618.618 0 00.374-.155l8.243-7.418a.618.618 0 10-.795-.947l-.029.026-7.835 7.045-7.827-7.045a.618.618 0 00-.854.895l.029.026 8.243 7.418a.618.618 0 00.451.155z"
                data-name="Path 5366"
                transform="translate(-6.928 -28.972)"
            ></path>
            </svg>
        )
    }
}