const API_URL = `https://api.takeshape.io/project/606038f1-bbe7-4e2a-bf17-da2f9f8350b6/graphql`
const API_KEY = '7ea942a44cdd4c79b0fed4c7369ca9ee'

async function fetchAPI(query, { variables } = {}) {
    const res = await fetch(API_URL, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${API_KEY}`,
        },
        body: JSON.stringify({
        query,
        variables,
        }),
    })

    const json = await res.json()
    if (json.errors) {
        console.error(json.errors)
        throw new Error('Failed to fetch API')
    }
    return json.data
}



export async function getPreviewPostById(id) {
    const data = await fetchAPI(
        `
            query PostBySlug($id: ID!) {
                post: getArticles(_id: $id) {
                    title,
                    _createdAt,
                    _id,
                    contentType{
                        title
                        slug
                    },
                    content,
                    featuredImage{
                        path
                    }
                    category{
                        title
                        slug
                    },
                    correspondent{
                        name
                        jobDescription,
                        shortBio,
                        image{
                            path
                        }
                    },
                    tags{
                        name,
                        slug
                    },
                    emojis{
                        alreadyReacted,
                        cry,
                        fire,
                        laugh,
                        poop
                    },
                    bookmarked,
                    relatedArticles{
                        title
                        _id
                        featuredImage{
                            path
                        }
                        category{
                            slug
                        }
                        contentType{
                            slug
                        }
                        tags{
                            _id
                            name
                            slug
                        }
                    },
                    comments{
                        name,
                        comment,
                        image{
                            path
                        }
                    }
                    files{
                        title
                        source
                        day
                        date
                        comment
                    }
                }
            }
        `,
        {
            variables: {
                id,
            },
        }
    )
    //console.log(data.post)
    return (data)
}

export async function getAllTags(){
    const data = await fetchAPI(
        `
            {
                allTags: getTagsList {
                    items {
                        _id
                        name
                        slug
                    }
                }
            }
        `
    )
    return data?.allTags?.items
}

export async function getAllPostsWithId() {
    const data = await fetchAPI(
        `
            {
                allPosts: getArticlesList {
                    items {
                        _id
                        category{
                            slug
                        },
                        contentType{
                            slug
                        }
                    }
                }
            }
        `
    )
    return data?.allPosts?.items
}


export async function getEditorChoices(){
    const data = await fetchAPI(
        `
            {
                editorChoices: geteditorsChoices{
                    articles{
                        title
                        excerpt
                        featuredImage{
                            path
                        }
                        category{
                            slug
                        }
                        contentType{
                            slug
                        }
                        tags{
                            slug
                            name
                        }
                    }
                }
            }
        `,
    )
    return data?.editorChoices?.articles
}


export async function getArticlesByCategory(category){
    const data = await fetchAPI(
        `
            query{
                articles: getArticlesList(
                    where: {
                        category :{
                            slug :{
                                match : "${category}"
                            }
                        }
                    }
                ){
                    items {
                        title
                        featuredImage{
                            path
                        }
                        category {
                            title
                            slug
                        }
                        contentType{
                            slug
                            title
                        }
                        tags{
                            name
                            slug
                        }
                        _id
                    }
                }
            }
        `
        ,
        {
            variables: {
                category,
            },
        }
    )
    return (data)
}


export async function getAllPostsForHome(preview) {
    const data = await fetchAPI(
        `
        query AllPosts($onlyEnabled: Boolean) {
            allArticles: getArticlesList(sort: { field: "date", order: "desc" }, size: 20, onlyEnabled: $onlyEnabled) {
                items {
                    _id,
                    title,
                    featuredImage{
                        path
                    },
                    slug,
                    category{
                        title,
                        slug
                    },
                    contentType{
                        title,
                        slug
                    },
                    tags{
                        name,
                        slug,
                        _id
                    }
                    
                }
            }
        }
        
    `,
        {
        variables: {
            onlyEnabled: true,
            preview,
        },
        }
    )
    return data?.allArticles?.items
}

