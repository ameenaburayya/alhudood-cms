export const EXAMPLE_PATH = 'cms-buttercms'
export const CMS_NAME = 'شبكة الحدود'
export const CMS_URL = 'https://alhudoodnextjs.vercel.app'
export const URL_SLUG = "alhudoodnextjs.vercel.app"
export const posts_to_show = 5
export const HOME_OG_IMAGE_URL =
  'https://og-image.now.sh/Next.js%20Blog%20Example%20with%20**ButterCMS**.png?theme=light&md=1&fontSize=100px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Fv1591431148%2Fnextjs%2Fexamples%2Fbuttercms-logo.svg'
