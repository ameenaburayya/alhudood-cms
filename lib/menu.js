export const FooterMenu = [
    {
        label: "من نحن", 
        link: "/about-us"
    },
    {
        label: "السياسة التحريرية" , 
        link: "/editorial-policy"
    },
    {
        label: "مبادئ الحدود الإحدى عشر" , 
        link: "/eleven-principles"
    },
    {
        label: "سياسة الخصوصية" , 
        link: "/privacy-policy"
    },
    {
        label: "اتصل بنا", 
        link: "/contact-us"
    },
    {
        label: "الداعمون", 
        link: "/supporters"
    }
]

export const HeaderMenuList = [
    {
        label: "محتمع الحدود", 
        link: "/about-us"
    },
    {
        label: "جَحُصَعْ" , 
        link: "/editorial-policy"
    },
    {
        label: "الحصاد الإعلامي" , 
        link: "/eleven-principles"
    }
]

export const SideMenuList = [
    {
        label: "في الأخبار",
        link: "/"
    },
    {
        label: "ليس في الأخبار",
        link: "/"
    },
    {
        label: "أوراق الحدود",
        link: "/"
    },
    {
        label: "جَحُصَعْ",
        link: "/"
    },
    {
        label: "مجتمع الحدود",
        link: "/"
    },
    {
        label: "إعلام ووساخات اخرى",
        link: "/"
    },
    {
        label: "السوق السوداء",
        link: "/"
    },
]


export const SideMenuSecList = [
    {
        label: "من نحن",
        link: "/"
    },
    {
        label: "اتصل بنا",
        link: "/"
    },
    {
        label: "السياسة التحريرية",
        link: "/"
    }
]