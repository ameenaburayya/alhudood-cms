
import { editor_choices } from '../../../public/data'

export default function handler(req, res) {
  res.status(200).json(editor_choices)
}