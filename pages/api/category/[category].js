import { posts } from '../../../public/data'

export default function categoryHandler({ query: { category } }, res) {
  const filtered = posts.filter((p) => p.category.slug === category)
  // User with id exists
  if (filtered.length > 0) {
    res.status(200).json(filtered)
  } else {
    
    res.status(404).json({ message: `User with id: ${category} not found.` })
  }
}