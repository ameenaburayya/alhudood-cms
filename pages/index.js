import Head from 'next/head'
import LatestPosts from '../components/posts/latestPosts'
import Header from '../includes/header/header'
import useSWR from 'swr'
import { getAllPostsForHome } from '../lib/api'


//const fetcher = (url) => fetch(url).then((res) => res.json())

export default function Home({ allArticles, preview }) {

    //const { data, error } = useSWR('/api/posts', fetcher)

    // if (error) return <div>Failed to load</div>
    // if (!data) return <div>Loading...</div>
    allArticles.forEach(function(post){
        post['id'] = post._id
    })
    return (
        <>
            <Header />
            <LatestPosts posts={allArticles} />
        </>
    )
}


// export async function getStaticProps() {
//     const res = await fetch('https://staging.alhudoodcms.alhudoodnet.com/api/tv1/articles')
//     const data = await res.json()
    
//     return { 
//         props: {
//         res,
//         }
//     }
// }

export async function getStaticProps({ preview = true }) {
    const allArticles = await getAllPostsForHome(preview)
    return {
        props: { allArticles, preview },
    }
  }
