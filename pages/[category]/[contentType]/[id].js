import { useRouter } from 'next/router'
import useSWR from 'swr'
import ErrorPage from 'next/error'
import PostLayout from '../../../components/layouts/postLayout'
import PostTemplate from '../../../templates/postTemplate'

import { getAllPostsWithId, getPreviewPostById } from '../../../lib/api'


// const fetcher = async (url) => {
//     const res = await fetch(url)
//     const data = await res.json()
  
//     if (res.status !== 200) {
//       throw new Error(data.message)
//     }
//     return data
// }


export default function Post({post}) {

    const { query } = useRouter()
    const router = useRouter()
    // const { data, error } = useSWR(
    //     () => query.id && `../../api/posts/${query.id}`,
    //     fetcher
    // )

    //const post = data

    // if (error) return <div>{error.message}</div>
    // if (!data) return <div>Loading...</div>

    console.log(post)
    
    if (!router.isFallback && !post?._id) {
        return <ErrorPage statusCode={404} />
    }
    return (
        <PostLayout>
            <article>
                <PostTemplate post={post} />
            </article>
        </PostLayout>
    )
}




// export async function getStaticPaths() {
//     // Call an external API endpoint to get posts
//     const res = await fetch('https://staging.alhudoodcms.alhudoodnet.com/api/tv1/articles')
//     const posts = await res.json()
  
//     // Get the paths we want to pre-render based on posts
//     const paths = posts.map((post) => ({
//       params: { slug: post.id.toString() },
//     }))
  
//     // We'll pre-render only these paths at build time.
//     // { fallback: false } means other routes should 404.
//     return { paths, fallback: false }
// }

// export async function getStaticProps({ params }) {
//     // params contains the post `id`.
//     // If the route is like /posts/1, then params.id is 1
//     const res = await fetch(`https://staging.alhudoodcms.alhudoodnet.com/api/tv1/articles/${params.slug}`)
//     const post = await res.json()
  
//     // Pass post data to the page via props
//     return { props: { post } }
//   }





// export async function getStaticPaths() {
//     const allPosts = await getAllPostsWithId()
//     return {
//         paths: allPosts?.map((post) => `/[${post.category.slug}]/[${post.contentType.slug}]/${post._id}`) || [],
//         fallback: true,
//     }
// }

export async function getStaticPaths() {
    const allPosts = await getAllPostsWithId()
    const paths = allPosts.map((post) => ({
        params: { 
            id: post._id ,
            category: post.category.slug,
            contentType: post.contentType.slug
        },
    }))
    return {
        paths,
        fallback: true,
    }
}


export async function getStaticProps({ params, preview = false }) {
    
    const data = await getPreviewPostById(params.id)
    // const content = await markdownToHtml(
    //     (data?.post?.items || [])[0]?.content || ''
    // )
    return {
        props: {
            preview,
            post: {
                ...(data?.post || [])
            }
        },
    }
}
