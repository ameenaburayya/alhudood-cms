import Header from '../../includes/header/header'
import TagsTicker from '../../components/tagsTicker';
import HashIcon from '../../components/icons/hashIcon';
import Sorter from '../../components/postSorter'
import {posts_to_show} from '../../lib/constants'
import DefaultErrorPage from 'next/error'
import { useState, useEffect } from 'react';
import PostsBlock from '../../components/posts/postsBlock';
import useSWR from 'swr'
import { useRouter } from 'next/router'
import SecondaryButton from '../../components/buttons/secondaryButton';
import MainHeading from '../../components/headings/mainHeading';
import EditorChoices from '../../components/editorChoices';
import Dal from '../../components/icons/dal';

import { getAllTags , getAllPostsWithId, getArticlesByCategory, getEditorChoices} from '../../lib/api'


// const fetcher = async (url) => {
//   const res = await fetch(url)
//   const data = await res.json()

//   if (res.status !== 200) {
//     throw new Error(data.message)
//   }
//   return data
// }


var categories = [
    {
        id: '42345',
        name: 'في الاخبار',
        slug: 'current_affairs',
    },
    {
        id: '234562',
        name: 'منوعات',
        slug: 'mix',
    },
    {
        id: '572345',
        name: 'ليس في الاخبار',
        slug: 'non-news-news',
    },
    {
        id: '92345',
        name: 'حصاد الإعلام',
        slug: 'media_monitoring',
        
    },
    {
        id: '892345',
        name: 'الحدودبيديا',
        slug: 'alhudood_media',
    },
    {
        id: '10484',
        name: 'بصري',
        slug: 'adfasdf',
    },

]

export default function Category({categoryData}) {

    const data = categoryData.data
    const editor_choices = categoryData.editorChoices
    const tags = categoryData.allTags
    const {query} = useRouter()

//   const { data, error } = useSWR(
//       () => query.category && `../../api/category/${query.category}`,
//       fetcher
//   )



  // this function is for load more button (this should be on a separate component)
    const [visibleBlogs, setVisibleBlogs] = useState(posts_to_show)
    const handleMorePosts = () => {
        setVisibleBlogs(prevVisibleBlogs => prevVisibleBlogs + posts_to_show)
    }
  //

  // you have to change the url when filtering, and check the url before render the page
  // update: now i can know if there is a filter query coming on page loading, now you have to do something when there is a filter query, also, you need to change the url query when the user select a type of filtering
    const router = useRouter()


    useEffect(() => {
        handleFilteringQuery()
    });


    function handleFilteringQuery(){
        if (router.query.filter){
            console.log(router.query.filter)
            return router.query.filter
        }
        else{
            return null
        }
    }

  //

  // the problem here is the hook is applied before the data is ready, it should work when there is an api and using gets static props and git static paths
    const [categoryPosts, setData] = useState(data);



    function handleState(newData) {
        setData(newData);
    }

  // getting the query category name
    const current_category = categories.filter((p) => p.slug == query.category)[0]



    // if (error) {
    //     return <DefaultErrorPage statusCode={404} />
    // }
    if (!data) return <div>Loading...</div>

    return (
        <>
            <Header />
            <div className="archive-page">
                <div className="archive-tags">
                    <div className="hash-icon">
                        <HashIcon />
                    </div>
                    <TagsTicker postTags={tags} />
                </div>
                <div className="archive-editor-choices">
                    <MainHeading label="من اختيار المحرر" />
                    <EditorChoices data={editor_choices} />
                    <Dal />
                </div>
                <div className="archive-posts">
                    <div className="archive-posts-header"> 
                        <MainHeading label={current_category.name} />
                        {/* <Sorter onChangeValue={handleState} types={current_category.content_types.types} category={query.category} filter={handleFilteringQuery} data={categoryPosts ? categoryPosts : data} /> */}
                        <Sorter onChangeValue={handleState}  category={query.category} filter={handleFilteringQuery} data={categoryPosts ? categoryPosts : data} />
                    </div>
                
                    <div className="archive-posts-list">
                        <PostsBlock image_shape="wide" archive data={categoryPosts ? categoryPosts : (data.slice(0 , visibleBlogs))} />
                    </div>
                </div>
                {((data.length - visibleBlogs) > 0) ? <SecondaryButton rounded color="black" onClick={handleMorePosts} label="المزيد" /> : null}
            </div> 
        </>
    )
}



export async function getStaticPaths() {
    const allPosts = await getAllPostsWithId()
    const paths = allPosts.map((post) => ({
        params: { 
            category: post.category.slug,
        },
    }))
    return {
        paths,
        fallback: true,
    }
}


export async function getStaticProps({ params, preview = false }) {
    const allTags = await getAllTags(preview)
    const editorChoices = await getEditorChoices()
    const data = await getArticlesByCategory(params.category)

    return {
        props: {
            preview,
            categoryData: {
                data: (data?.articles?.items || []), 
                allTags : allTags,
                editorChoices : editorChoices
            }
        },
    }
}


// export async function getStaticProps() {
//     const res = await fetch('https://staging.alhudoodcms.alhudoodnet.com/api/tv1/articles')
//     const posts = await res.json()
//     return {
//         props: { posts },
//     }
// }
