import PostHeader from '../../components/singlePost/postHeader'
import PostBody from '../../components/singlePost/postBody'

export default function DefaultPostTemplate({post}){

    const postTitle = post.title
    const postId = post.id
    const postDate = post.date_ar
    const postBookmark = post.bookmark
    const postCorrespondent = {
        name: post.correspondent.name,
        description: post.correspondent.jobDescription
    }
    const postCategory = post.category.name

    return (
        <div className="post-inner default-post">
            <PostHeader bookmark={postBookmark} id={postId} category={postCategory} correspondent={postCorrespondent} date={postDate} title={postTitle} />
            <PostBody post={post} />
        </div>
    )
}