import React, {useRef, useEffect, Component} from 'react'

import Files from './mediaMonitoring/files'
import Fe9am from './mediaMonitoring/fa9am'
import Mokaf7a from './mediaMonitoring/mokaf7a'
import Correction from './mediaMonitoring/correction'


// const window_height = window.innerHeight;

export default class MediaMonitoringTemplate extends Component {

    constructor(props){
        super(props)
        this.handleBoxBottom = this.handleBoxBottom.bind(this)
        this.child = React.createRef()
    }

    handleBoxBottom = () => {
        const box_position_bottom = this.child.current.box.current.getClientRects()[0].bottom
        if(box_position_bottom < window.innerHeight){
            this.child.current.postHeader.current.classList.add('done')
        }
        else{
            this.child.current.postHeader.current.classList.remove('done')
        }
    }

    componentDidMount() {
        {this.props.post.contentType.slug !== "correction" && window.addEventListener('scroll', this.handleBoxBottom)};
    }
    
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleBoxBottom);
    }

    render(){
        
        switch (this.props.post.contentType.slug) {
            case "files": return <Files ref={this.child} post={this.props.post} />
            case "fe9am": return <Fe9am ref={this.child} post={this.props.post} />;
            case "mokaf7a":  return <Mokaf7a ref={this.child} post={this.props.post} />;
            case 'correction': return <Correction post={this.props.post} />
            default: return <h1>hello</h1>;
        }
    }
}
