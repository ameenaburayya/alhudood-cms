import React, { Component } from 'react'
import DangerIcon from '../../../components/icons/dangerIcon'
import EyeIcon from '../../../components/icons/eyeIcon'
import FingerprintIcon from '../../../components/icons/fingerprintIcon'
import GavelIcon from '../../../components/icons/gavelIcon'
import PostButtons from '../../../components/postButtons';
import {getImageUrl} from '@takeshape/routing'

export default class Mokaf7a extends Component {

    constructor(props) {
        super(props);
        this.box = React.createRef()
        this.postHeader = React.createRef()
    }

    render() {
        const post = this.props.post
        return (
            <div className="post-inner media-monitoring mokaf7a">
                <div className="post-info">
                    <div className="post-info-inner">
                        <div className="post-type">{post.content_type.name}</div>
                        <PostButtons />
                    </div>
                </div>
                <div ref={this.postHeader} className="post-header">
                    <div className="post-image">
                        <figure>
                            <picture>
                                <img src={getImageUrl(post.featuredImage.path)} />
                            </picture>
                        </figure>
                    </div>
                    <div className="post-title">
                        <div className="criminal">
                            {post.mokaf7a.criminal}
                        </div>
                        <div className="mokaf7a-title">
                            المجرم
                        </div>
                    </div>
                </div>
                <div ref={this.box} className="post-body">
                    <div className="post-body-inner">
                        <div className="mokaf7a-section crime">
                            <div className="section-title">الجريمة</div>
                            <div className="section-content">
                                {post.mokaf7a.crime}
                            </div>
                            <div className="section-icon">
                                <EyeIcon />
                            </div>
                        </div>
                        <div className="mokaf7a-section report">
                            <div className="section-title">محضر الضبط</div>
                            <div className="section-content">
                                {post.mokaf7a.seizure_report}
                            </div>
                            <div className="section-icon">
                                <FingerprintIcon />
                            </div>
                        </div>
                        <div className="mokaf7a-section severity">
                            <div className="section-title">درجة الخطورة</div>
                            <div className="section-content">
                                {post.mokaf7a.severity}
                            </div>
                            <div className="section-icon">
                                <DangerIcon />
                            </div>
                        </div>
                        <div className="mokaf7a-section rehabilitation">
                            <div className="section-title">منهجية الدولة في إعادة التأهيل</div>
                            <div className="section-content">
                                {post.mokaf7a.rehabilitation}
                            </div>
                            <div className="section-icon">
                                <GavelIcon />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
