import React, { useRef, useState, useEffect, createRef, Component } from 'react'
import Dal from '../../../components/icons/dal'
import PostButtons from '../../../components/postButtons';
import { getMediaMonitoringPosts , getPreviewPostById } from '../../../lib/api'
import {getImageUrl} from '@takeshape/routing'


var post_header_height
var last_box_height 
var pin_position_limit
var window_height

export default class Files extends Component {

    constructor(props){
        super(props)
        this.parent = React.createRef()
        this.box = React.createRef()
        this.postHeader = React.createRef()
        this.elRefs = []
        this.handlePositionTop = this.handlePositionTop.bind(this)
        this.state = {
            position: null,
            pinClass: null,
        }
    }


    //const arrLength = post.files.length;
    

    handlePositionTop(){
        const element_position = this.box.current.getClientRects()[0].top
        var pin_position = post_header_height -  element_position + 65
        pin_position = pin_position > pin_position_limit ? pin_position_limit : pin_position;

        // elRefs.map((box, index) => {
        //     const box_position = box.current.getClientRects()[0].top - element_position.top
        //     var before_box_position = null
        //     var after_box_position = null
        //     if((index - 1) >= 0) {
        //         //console.log(index - 1)
        //         before_box_position = (elRefs[index - 1].current.getClientRects()[0]).top - element_position.top
        //     }
        //     if(index < (elRefs.length - 1)){
                
        //         after_box_position = (elRefs[index + 1].current.getClientRects()[0]).top - element_position.top
        //     }

        //     if(pin_position == after_box_position || pin_position == before_box_position ){
        //         setanything(true)
        //         elRefs[index].current.classList.remove('active')
        //     } else if (pin_position == box_position){
        //         elRefs[index].current.classList.add('active')
        //     }
            
        // })
        

        this.setState({
            position: pin_position
        })
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handlePositionTop);

        post_header_height = this.postHeader.current.getClientRects()[0].height    
        last_box_height = this.elRefs.slice(-1)[0].getClientRects()[0].height
        pin_position_limit = this.box.current.getClientRects()[0].height - last_box_height


        window_height = window.innerHeight;

        document.documentElement.style.setProperty("--window-height", window_height); 
    }
    
    componentWillUnmount() {
        window.removeEventListener('scroll', this.handlePositionTop);
    }



    render(){
        const post = this.props.post
        return (
            <div ref={this.parent} className="post-inner media-monitoring files">
                <div className="post-info">
                    <div className="post-info-inner">
                        <div className="post-type">{post.contentType.name}</div>
                        <PostButtons />
                    </div>
                </div>
                <div ref={this.postHeader} className="post-header">
                    <div className="post-image">
                        <figure>
                            <picture>
                                <img src={getImageUrl(post.featuredImage.path)} />
                            </picture>
                        </figure>
                    </div>
                    <div className="post-title">{post.title}</div>
                </div>
                <div ref={this.box} className="post-body">
                    <div className="post-body-inner">
                        {post.files.map((file, index) =>{
                            return(
                                <div ref={(ref) => this.elRefs[index] = ref} key={index} className="file">
                                    <div className="file-info">
                                        <div className="file-source">{file.source}</div>
                                        <div className="file-time">
                                            <div className="file-day">{file.day}</div>
                                            <div className="file-date">{file.date}</div>
                                        </div>
                                    </div>
                                    <div className="file-content">
                                        <div className="file-title">{file.title}</div>
                                        <div className="file-comment">
                                            <Dal />
                                            <div className="file-comment-inner">{file.comment}</div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                        <div className="timeline">
                            <div style={{top: this.state.position}} className={`timeline-pin stable ${this.state.pinClass ? "stable" : "moving"}`}>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


