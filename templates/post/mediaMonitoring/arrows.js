

    window.initHowToBuildApiPage = function($) {
        var selectedPhase = 0;

        var calculateLine = function(phase) {
            var left, phaseStepsLineOffset, right, stateToCircleOffset, width;

            stateToCircleOffset = $('#' + phase).find('.stateToCircle').offset();
            phaseStepsLineOffset = $('.selectedPhase .phaseStepsContent').offset();

            left = Math.min(stateToCircleOffset.left, phaseStepsLineOffset.left);
            right = Math.max(stateToCircleOffset.left, phaseStepsLineOffset.left);

            width = right - left;

            $('.phaseToStepsLine svg').offset({
                left: left
            }).width(width);
        };

        var slideToTarget = function(target, topOffset, speed) {
            if (topOffset === undefined) {
                topOffset = $("#websiteHeader").outerHeight() + ($("#websiteHeader").outerHeight() / 10);
            }
            if (speed === undefined) {
                speed = 1000;
            }

            if (target.length) {
                var page = $("html, body");
                page.on("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove", function(){
                    page.stop();
                });

                page.animate({ scrollTop: target.offset().top - topOffset }, 'slow', function(){
                    page.off("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove");
                });

                return false;
            }
        };

        var changePhase = function(phase, writeToHistory) {
            if (writeToHistory === undefined) {
                writeToHistory = true;
            }

            $(".selectedPhase").removeClass('selectedPhase');
            $("#"+phase).addClass('selectedPhase');
            $(".phaseStepsContainer#content-" + phase).addClass('selectedPhase');

            var phases = $(".phase");

            var k = 0;
            $.each(phases, function(key, item){
               if($(item).hasClass('selectedPhase')){
                   selectedPhase = k;
               }
               k++;
            });

            calculateLine(phase);

            if (writeToHistory) {
              try {
                history.pushState({}, "", '#'+phase);
              }
              catch (e) {
                //intentional error ignoring
              }
              slideToTarget($(".buildDiagram"));
            }

            //Send pageview for particular part, GA doesn't play with hash parts
            ga('send', 'pageview', '/' + phase)
        }

        var goToHash = function(hash){
            if(hash.length) {
                var phase, step;

                if($(".phase"+hash).length > 0){
                    phase = hash.substring(1);
                } else {
                    step = $(".phaseStep[data-open="+hash+"]");

                    step.addClass('open');
                    $(step.data('open')).show();
                    if(step.length > 0){
                        phase = step.closest(".phaseStepsContainer").attr('id').replace('content-', "");
                    }
                }
                if(phase.length > 0){
                    changePhase(phase, false);
                }
                if(step !== undefined && step.length > 0){
                    slideToTarget(step);
                } else {
                    if(phase.length > 0){
                        slideToTarget($(".buildDiagram"));
                    }
                }
            }
        }

        $(document).ready(function () {
            var hash;
            hash = window.location.hash;
            goToHash(hash);

            calculateLine($(".phase.selectedPhase").first().attr('id'));

            $(document).on("click", ".phaseCircle, .phaseState", function(e){
                var parent = $(this).closest(".phase");
                changePhase(parent.attr('id'));
            });

            $(document).on("click", ".phaseStep", function(e){
                var content = $(this).data('open');
                try {
                  history.pushState({}, "", content);
                }
                catch (e) {
                  //intentional error ignoring
                }
                $(content).slideToggle();
                $(this).toggleClass('open');
            });

            $(document).on("click", ".changePhase", function(e){
                var href = $(this).attr('href').substring(1);
                changePhase(href);
                e.preventDefault();
            });

            $(document).on("click", ".prevNextBtn.nextBtn", function(e){
                var $phases = $(".buildPhases > .phase");
                selectedPhase = selectedPhase + 1;
                if(selectedPhase >= 6){
                    selectedPhase = 0;
                }
                changePhase($($phases[selectedPhase]).attr('id'));
                e.preventDefault();
            });

            $(document).on("click", ".prevNextBtn.prevBtn", function(e){
                var $phases = $(".buildPhases > .phase");
                selectedPhase = selectedPhase - 1;
                if(selectedPhase < 0){
                    selectedPhase = 5;
                }
                changePhase($($phases[selectedPhase]).attr('id'));
                e.preventDefault();
            });

            $(document).on("click", ".phaseStepArticle a[href*=#]", function(e){
                e.preventDefault();
                goToHash($(e.target).attr('href'));
            });

            $(document).on("click", ".p-share a", function(e){
                $(e.target).parent().data('share-open', 'false');
                $(e.target).remove();
            });
            $(document).on("click", ".p-share", function(e){
                var $target = $(e.target);
                var linkVisible = $target.data('share-open') === 'true';

                if(!linkVisible){
                    $target.data('share-open', 'true');
                    var text = '"'+$target.text()+'" '+window.location.href+' via @apiaryio';
                    $target.append("<a href='http://twitter.com/home?status=" +escape(text)+ "' target='_blank'><span></span>Share on Twitter</a>");
                } else {
                    $target.find('a').remove();
                    $target.data('share-open', 'false');
                }
            });
        });

        $(window).resize(function() {
            calculateLine($(".phase.selectedPhase").first().attr('id'));
        });
    }