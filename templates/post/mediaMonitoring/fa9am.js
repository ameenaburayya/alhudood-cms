import React, {useState , useEffect ,useRef} from 'react'
import QuoteIcon from '../../../components/icons/quoteIcon';
import PostButtons from '../../../components/postButtons';
import ExpandIcon from '../../../components/icons/expandIcon'
import Dal from '../../../components/icons/dal';
import {getImageUrl} from '@takeshape/routing'


const wantedHeight = 121
export default class Fa9am extends React.Component {

    constructor(props) {
        super(props);
        this.handleBeforeExpand = this.handleBeforeExpand.bind(this)
        this.handleAfterExpand = this.handleAfterExpand.bind(this)
        this.before = React.createRef();
        this.after = React.createRef();
        this.afterTitle = React.createRef();
        this.beforeTitle = React.createRef();
        this.box = React.createRef()
        this.postHeader = React.createRef()
        this.state = {
            beforeHeight: 0,
            beforeExpand: false,
            afterHeight: 0,
            afterExpand: false,
        };
    }

    handleBeforeExpand(){
        this.setState({
            beforeExpand: !this.state.beforeExpand
        })
    }

    handleAfterExpand(){
        this.setState({
            afterExpand: !this.state.afterExpand
        })
    }

    componentDidMount() {
        const beforeHeight = this.before.current.clientHeight;
        const afterHeight = this.after.current.clientHeight;
        const titleHeight = Math.max(this.beforeTitle.current.clientHeight, this.afterTitle.current.clientHeight);
        this.setState({ beforeHeight, afterHeight });
        document.documentElement.style.setProperty('--excerptHeight', (wantedHeight + 'px'));
        document.documentElement.style.setProperty('--beforeHeight', (beforeHeight + 'px'));
        document.documentElement.style.setProperty('--afterHeight', (afterHeight + 'px'));
        document.documentElement.style.setProperty('--titleHeight', (titleHeight + 'px'));
    }


    render (){
        const post = this.props.post
        return (
            <div className="post-inner media-monitoring fe9am">
                <div className="post-info">
                    <div className="post-info-inner">
                        <div className="post-type">{post.content_type.name}</div>
                        <PostButtons />
                    </div>
                </div>
                <div ref={this.postHeader} className="post-header">
                    <div className="post-image">
                        <figure>
                            <picture>
                                <img src={getImageUrl(post.featuredImage.path)} />
                            </picture>
                        </figure>
                    </div>
                    <div className="post-title">
                        <div className="fe9am-logo">
                            <img src={getImageUrl(post.featuredImage.path)} />
                        </div>
                        <div className="fe9am-source">
                            <span>{post.fe9am.name}</span>، <span>{post.fe9am.country}</span>
                        </div>
                    </div>
                </div>
                <div ref={this.box} className="post-body">
                    <div className="post-body-inner">
                        <div className="fe9am-content">
                            <div className="fe9am-before fe9am-content-item">
                                <div className="fe9am-heading">في يوم</div>
                                <div className="fe9am-date">{post.fe9am.before.date}</div>
                                <div className="fe9am-pronoun">{post.fe9am.before.pronoun}</div>
                                <div className="fe9am-title">
                                    <div ref={this.beforeTitle}  className="fe9am-title-inner">
                                        {post.fe9am.before.title}
                                    </div>
                                    <div className="quote-icon"><QuoteIcon /></div>
                                </div>
                                <div className={`fe9am-excerpt${this.state.beforeExpand ? " expanded" : ""}`}>
                                    <div className="excerpt-heading">وجاء في المقال</div>
                                    <div className="inner-excerpt">
                                        <div ref={this.before} dangerouslySetInnerHTML={{ __html: post.fe9am.before.excerpt  }}></div>
                                    </div>
                                    {this.state.beforeHeight > wantedHeight  ? <span className="expand"><span onClick={this.handleBeforeExpand} className="arrow"></span></span> : null}
                                </div>
                            </div>
                            <div className="fe9am-after fe9am-content-item">
                                <div className="fe9am-heading">في يوم</div>
                                <div className="fe9am-date">{post.fe9am.after.date}</div>
                                <div className="fe9am-pronoun">{post.fe9am.after.pronoun}</div>
                                <div  className="fe9am-title">
                                    <div ref={this.afterTitle} className="fe9am-title-inner">
                                        {post.fe9am.after.title}
                                    </div>
                                    <div className="quote-icon"><QuoteIcon /></div>
                                </div>
                                <div className={`fe9am-excerpt${this.state.afterExpand ? " expanded" : ""}`}>
                                    <div className="excerpt-heading">وجاء في المقال</div>
                                    <div className="inner-excerpt">
                                        <div ref={this.after} dangerouslySetInnerHTML={{ __html: post.fe9am.after.excerpt  }}></div>
                                    </div>
                                    {this.state.afterHeight > wantedHeight  ? <span className="expand"><span onClick={this.handleAfterExpand} className="arrow"></span></span> : null}
                                </div>
                            </div>
                        </div>
                        <div className="fe9am-comment">
                            <span>فكتبت الحدود</span>
                            {post.fe9am.alhudood}
                            <Dal />
                        </div>
                    </div>
                </div>   
            </div>
            
        )
    }
}
