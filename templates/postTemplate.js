
import DefaultPostTemplate from './post/defaultPostTemplate'
import MediaMonitoringTemplate from './post/mediaMonitoringTemplate'
import Head from 'next/head';
import RelatedPosts from '../components/singlePost/relatedPosts'
import {getImageUrl} from '@takeshape/routing'


export default function PostTemplate({post}) {

    const postTitle = post.title
    const postDescription = post.excerpt
    const postImage = getImageUrl(post.featuredImage.path)
    const postId = post.id
    const postRelated = post.relatedArticles    
    return (
        <>
            <Head>
                <title>{postTitle}</title>
                
                <meta name="description" content={postDescription} />

                <meta property="article:publisher" content="https://facebook.com/AlHudoodNet/" />

                <meta property="og:type" content="article" />
                <meta property="og:title" content={postTitle} />
                <meta property="og:description" content={postDescription} />
                <meta property="og:image" content={postImage} />
                <meta property="og:url" content={`https://alhudood.net/${postId}`} />


                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content={postTitle} />
                <meta name="twitter:description" content={postDescription} />
                <meta name="twitter:image" content={postImage} />
            </Head>
            
            <>
                {/* // you should use switch not if */}
                
                {post.category.slug == "media_monitoring" 
                ? <MediaMonitoringTemplate post={post} />
                : <DefaultPostTemplate post={post} />
                }

                <RelatedPosts posts={postRelated} />
            </>
        </>
    )
}

